package ru.mirsaitov.tm.repository;

import ru.mirsaitov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    /**
     * Create project by name
     *
     * @param name - name of project
     * @return created project
     */
    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    /**
     * Create project by name and description
     *
     * @param name        - name of project
     * @param description - description of project
     * @return created project
     */
    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    /**
     * Clear projects
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Get size of repository
     */
    public int getSize() {
        return projects.size();
    }

    /**
     * Find project by index
     *
     * @param index index of project
     * @return project or null
     */
    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    /**
     * Find project by name
     *
     * @param name name of project
     * @return project or null
     */
    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) {
                return project;
            }
        }
        return null;
    }

    /**
     * Find project by id
     *
     * @param id id of project
     * @return project or null
     */
    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) {
                return project;
            }
        }
        return null;
    }

    /**
     * Remove project by index
     *
     * @param index index of project
     * @return project or null
     */
    public Project removeByIndex(final int index) {
        Project project = findByIndex(index);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Remove project by name
     *
     * @param name name of project
     * @return project or null
     */
    public Project removeByName(final String name) {
        Project project = findByName(name);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Remove project by id
     *
     * @param id id of project
     * @return project or null
     */
    public Project removeById(final Long id) {
        Project project = findById(id);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name) {
        Project project = findByIndex(index);
        if (project == null) {
            return null;
        }
        project.setName(name);
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param description description of project
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name, final String description) {
        Project project = findByIndex(index);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @return project or null
     */
    public Project updateById(final Long id, final String name) {
        Project project = findById(id);
        if (project == null) {
            return null;
        }
        project.setName(name);
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @return project or null
     */
    public Project updateById(final Long id, final String name, final String description) {
        Project project = findById(id);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Return projects
     */
    public List<Project> findAll() {
        return new ArrayList<>(projects);
    }

}