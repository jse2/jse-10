package ru.mirsaitov.tm.entity;

public class Task {

    private Long id = System.nanoTime();

    private String name;

    private String description;

    private Long projectId;

    public Task() {
        this.name = "";
        this.description = "";
    }

    public Task(String name) {
        this.name = name;
        this.description = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

}
