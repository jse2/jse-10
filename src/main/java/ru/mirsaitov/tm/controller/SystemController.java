package ru.mirsaitov.tm.controller;

public class SystemController extends AbstractController {

    /**
     * Welcome information
     */
    public void displayWelcome() {
        System.out.println(bundle.getString("welcome"));
    }

    /**
     * Display version of program
     */
    public void displayVersion() {
        System.out.println(bundle.getString("version"));
    }

    /**
     * Display information about of program
     */
    public void displayAbout() {
        System.out.println(bundle.getString("about"));
    }

    /**
     * Display help
     */
    public void displayHelp() {
        System.out.println(bundle.getString("help"));
    }

    /**
     * Display stub
     */
    public void displayStub(final String line) {
        System.out.println(String.format(bundle.getString("stub"), line));
    }

}
