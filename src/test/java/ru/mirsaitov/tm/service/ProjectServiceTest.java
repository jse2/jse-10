package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Project;
import ru.mirsaitov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectServiceTest {

    private final String name = "name";

    private final String description = "description";

    private final ProjectService projectService = new ProjectService(new ProjectRepository());

    @Test
    public void shouldCreate() {
        Project project = projectService.create(name);
        assertEquals(name, project.getName());
        assertEquals(name, project.getDescription());

        project = projectService.create(name, description);
        assertEquals(name, project.getName());
        assertEquals(description, project.getDescription());
    }

    @Test
    public void shouldClear() {
        Project project = projectService.create(name);
        assertTrue(!projectService.findAll().isEmpty());

        projectService.clear();
        assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Project> projects = new ArrayList<>();

        projects.add(projectService.create(name));
        assertTrue(projects.equals(projectService.findAll()));

        projects.add(projectService.create(name, description));
        assertTrue(projects.equals(projectService.findAll()));
    }

    @Test
    public void shouldFind() {
        Project project = projectService.create(name, description);

        assertEquals(projectService.findById(project.getId()), project);
        assertEquals(projectService.findByName(project.getName()), project);
        assertEquals(projectService.findByIndex(0), project);

        assertEquals(projectService.findById(1L), null);
        assertEquals(projectService.findByName("1"), null);
        assertEquals(projectService.findByIndex(1), null);
    }

    @Test
    public void shouldRemove() {
        Project project = projectService.create(name, description);
        assertEquals(projectService.removeById(project.getId()), project);

        project = projectService.create(name, description);
        assertEquals(projectService.removeByName(project.getName()), project);

        project = projectService.create(name, description);
        assertEquals(projectService.removeByIndex(0), project);

        assertEquals(projectService.removeById(1L), null);
        assertEquals(projectService.removeByName("1"), null);
        assertEquals(projectService.removeByIndex(1), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Project project = projectService.create(name, description);

        assertEquals(projectService.updateByIndex(0, updateName), project);
        assertEquals(project.getName(), updateName);

        assertEquals(projectService.updateById(project.getId(), updateDescription, updateName), project);
        assertEquals(project.getName(), updateDescription);
        assertEquals(project.getDescription(), updateName);
    }

}