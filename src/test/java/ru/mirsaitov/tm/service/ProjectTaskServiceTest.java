package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.constant.TerminalConst;
import ru.mirsaitov.tm.entity.Project;
import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.repository.ProjectRepository;
import ru.mirsaitov.tm.repository.TaskRepository;

import static org.junit.Assert.*;

public class ProjectTaskServiceTest {

    private final String name = "name";

    private final String description = "description";

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Test
    public void shouldAddTaskToProject() {
        Project project = projectService.create(name, description);
        Task task = taskService.create(name, description);

        assertEquals(task.getProjectId(), null);
        projectTaskService.addTaskToProject(project.getId(), task.getId());
        assertEquals(task.getProjectId(), project.getId());
    }

    @Test
    public void shouldRemoveProject() {
        Project project = projectService.create(name, description);
        Task task = taskService.create(name, description);

        assertEquals(task.getProjectId(), null);
        projectTaskService.addTaskToProject(project.getId(), task.getId());
        assertEquals(task.getProjectId(), project.getId());
        projectTaskService.removeProject(TerminalConst.OPTION_ID, project.getId().toString(), false);
        assertEquals(task.getProjectId(), null);
    }

    @Test
    public void shouldRemoveProjectAndTask() {
        Project project = projectService.create(name, description);
        Task task = taskService.create(name, description);

        assertEquals(task.getProjectId(), null);
        projectTaskService.addTaskToProject(project.getId(), task.getId());
        assertEquals(task.getProjectId(), project.getId());
        projectTaskService.removeProject(TerminalConst.OPTION_ID, project.getId().toString(), true);
        assertTrue(taskService.findAll().isEmpty());
    }

}
